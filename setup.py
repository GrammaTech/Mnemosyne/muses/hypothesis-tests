import setuptools

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='hypothesis-tests',
    author='Grammatech',
    description='LSP server generating tests using the hypothesis library',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/GrammaTech/Mnemosyne/muses/hypothesis-tests',
    packages=['hypothesis_tests'],
    package_dir={'':'src'},
    python_requires='>=3.8',
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.8',
    ],
    scripts=['bin/hypothesis-tests'],
    install_requires=[
        'astunparse',
        'dpcontracts',
        'hypothesis[dpcontracts]',
        'pygls',
        'pytest',
        'pytest-timeout'
    ]
)
