"""
Unit tests for the hypothesis tests muse.
"""

import ast
import astunparse
import pathlib

from typing import List, Optional, Tuple
from src.hypothesis_tests.lib import hypothesis_imports, \
                                     hypothesis_test, \
                                     falsifying_examples


def source(a: ast.AST) -> str:
    '''
    Return the source text corresponding to AST.
    '''
    return astunparse.unparse(a).strip()


def strategy(filename: str) -> str:
    '''
    Return the hypothesis strategy decorator for the function
    defined in FILENAME.
    '''
    examples_dir = pathlib.Path(__file__).parent / 'examples' / 'test-creation'
    with open(examples_dir / filename) as f:
        text = f.read()
        root = ast.parse(text)
        func = root.body[-1]
        test = hypothesis_test(func, root)

        return source(test.decorator_list[0])


def examples(filename) -> List[List[Tuple]]:
    '''
    Return the test failure examples for the hypothesis test
    defined in FILENAME.
    '''
    examples_dir = pathlib.Path(__file__).parent
    examples_dir = examples_dir / 'examples' / 'falsifying-examples'
    with open(examples_dir / filename) as f:
        text = f.read()
        root = ast.parse(text)
        test = root.body[-1]

        return falsifying_examples(test, root)


def dpcontracts_test_body(filename: str) -> str:
    '''
    Return the hypothesis test body for the function defined in FILENAME.
    '''
    examples_dir = pathlib.Path(__file__).parent / 'examples' / 'dpcontracts'
    with open(examples_dir / filename) as f:
        text = f.read()
        root = ast.parse(text)
        func = root.body[-1]
        test = hypothesis_test(func, root)

        return source(test.body[0])


def test_hypothesis_imports():
    imports = hypothesis_imports()

    assert len(imports) == 3
    assert source(imports[0]) == \
           'from hypothesis import given, example'
    assert source(imports[1]) == \
           'from hypothesis.extra.dpcontracts import fulfill'
    assert source(imports[2]) == \
           'import hypothesis.strategies as st'


def test_int_strategy():
    assert strategy('int.py') == 'given(a=st.integers())'


def test_float_strategy():
    assert strategy('float.py') == 'given(a=st.floats())'


def test_bool_strategy():
    assert strategy('bool.py') == 'given(a=st.booleans())'


def test_bytes_strategy():
    assert strategy('bytes.py') == 'given(a=st.binary())'


def test_str_strategy():
    assert strategy('str.py') == 'given(a=st.text())'


def test_bytestring_strategy():
    assert strategy('bytestring.py') == 'given(a=st.binary())'


def test_text_strategy():
    assert strategy('text.py') == 'given(a=st.text())'


def test_datetime_strategy():
    assert strategy('datetime.py') == 'given(a=st.datetimes())'


def test_time_strategy():
    assert strategy('time.py') == 'given(a=st.times())'


def test_timedelta_strategy():
    assert strategy('timedelta.py') == 'given(a=st.timedeltas())'


def test_decimal_strategy():
    assert strategy('decimal.py') == 'given(a=st.decimals())'


def test_fraction_strategy():
    assert strategy('fraction.py') == 'given(a=st.fractions())'


def test_random_strategy():
    assert strategy('random.py') == 'given(a=st.randoms())'


def test_uuid_strategy():
    assert strategy('uuid.py') == 'given(a=st.uuids())'


def test_list_strategy_1():
    assert strategy('list1.py') == 'given(a=st.lists(st.integers()))'


def test_list_strategy_2():
    assert strategy('list2.py') == 'given(a=st.lists(st.integers()))'


def test_list_strategy_3():
    assert strategy('list3.py') == 'given(a=st.lists(st.times()))'


def test_list_strategy_4():
    assert strategy('list4.py') == 'given(a=st.lists(st.times()))'


def test_set_strategy():
    assert strategy('set.py') == 'given(a=st.sets(st.integers()))'


def test_dict_strategy():
    assert strategy('dict.py') == \
           'given(a=st.dictionaries(st.text(), st.integers()))'


def test_iterable_strategy():
    assert strategy('iterable.py') == 'given(a=st.iterables(st.integers()))'


def test_tuple_strategy_1():
    assert strategy('tuple1.py') == \
           'given(a=st.tuples(st.integers(), st.floats(), st.text()))'


def test_tuple_strategy_2():
    assert strategy('tuple2.py') == \
           'given(a=st.tuples(st.times(), st.timedeltas(), st.datetimes()))'


def test_demo():
    assert strategy('demo.py') == 'given(l=st.lists(st.integers()))'


def test_falsifying_examples_1():
    assert examples('falsifying_examples1.py') == [[('a', 0)]]


def test_falsifying_examples_2():
    assert examples('falsifying_examples2.py') == [[('a', [])]]


def test_falsifying_examples_3():
    assert examples('falsifying_examples3.py') == [[('a', -11)], [('a', 11)]]


def test_dpcontracts_1():
    assert dpcontracts_test_body('dpcontracts1.py') == 'fulfill(f)(i)'


def test_dpcontracts_2():
    assert dpcontracts_test_body('dpcontracts2.py') == 'f(i)'
