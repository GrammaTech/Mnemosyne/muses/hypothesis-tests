from typing import List
from hypothesis import given
import hypothesis.strategies as st


def f(a: List[int]):
    return a + None


@given(a=st.lists(st.integers()))
def test_f(a):
    f(a)
