from hypothesis import given
import hypothesis.strategies as st


def f(a: int):
    return a + None


@given(a=st.integers())
def test_f(a):
    f(a)
