from hypothesis import given
import hypothesis.strategies as st


def f(a: int):
    if a < -10:
        return a + None
    elif a > 10:
        return a + 1 + None
    else:
        return a


@given(a=st.integers())
def test_f(a):
    f(a)
