from typing import List


def delete_ones(l: List[int]) -> None:
    for i in range(len(l)):
        if l[i] == 1:
            del l[i]
