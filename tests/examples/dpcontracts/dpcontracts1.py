from math import sqrt
from dpcontracts import require


@require("`i` is non-negative", lambda args: args.i >= 0)
def f(i: int) -> float:
    return sqrt(i)**2
