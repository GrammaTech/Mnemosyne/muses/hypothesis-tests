from math import sqrt


def f(i: int) -> float:
    return sqrt(i)**2
