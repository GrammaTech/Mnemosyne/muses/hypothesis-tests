# Type-driven hypothesis testing

Implementation of automated type-based hypothesis testing for python
programs with type annotations.

# Requirements

The hypothesis testing repository requires python 3.8 or higher.

# Installation

## Development

The dependencies for the repository may be installed by executing
`pip3 install -r requirements.txt`.  You must also place the
repository's src/ directory on your $PYTHONPATH.  From the base
of this repository, this may be accomplished with the following
command:

```
export PYTHONPATH=$PWD/src:$PYTHONPATH.
```

Additionally, you will likely wish to add the bin/ directory
to your $PATH by executing the following command:

```
export PATH=$PWD/bin:$PATH
```

## Production

You may create a pip wheel from this repository by executing
the following command:

```
python3 setup.py bdist_wheel --universal
```

The wheel may then be install via the following command:

```
pip3 install dist/hypothesis_tests-0.0.0-py2.py3-none-any.whl
```

# Command Line Interface

The `hypothesis-tests` script in the bin directory defines a command line
interface for this tool.  Help documentation is available by running
`hypothesis-tests --help`.

The script operates in two modes - LSP server mode and batch mode.  In LSP
server mode (the default), the script will listen for code action requests
and return code actions inserting hypothesis tests for the selected functions.
In batch mode (when the user passes --file), the script will read a file and
insert hypothesis tests for each function, where possible, writing the result
to standard out.

# Example

Open a file in the base of this repository and insert the following example
code:

```python
from typing import List

def delete_ones(l: List[int]) -> None:
    for i in range(len(l)):
        if l[i] == 1:
            del l[i]
```

Note that the code for `delete_ones` contains a bug; after executing
`del[i]` the length of the list l has changed.

After saving and closing the file, execute the following:

```
hypothesis-tests --file <filename>
```

After execution, you should see the file with a newly added hypothesis
test for `delete_ones` written to standard output.  You may save this
output to a file (either by redirecting standard output to a file or
copy-pasting the output into a file) and execute the new test by running
`pytest <file-w-test>`.

You may also use the `hypothesis-test` script in LSP mode to add the test
by connecting your code editor to the LSP server started by the script (by
default on port 3033), selecting (highlighting) the function to create a
test from, and requesting a code action for the function.  A code action
with the edits required for insertion of a hypothesis test will be returned.
Insructions for connecting your editor a LSP server differ by editor and
are too numerous to list here; the usage instructions for argot server,
given below, may be a useful primer.

# Argot Server

This muse was designed for use by the Argot LSP server.  Instructions for use
with the Argot LSP server are given in
https://grammatech.gitlab.io/Mnemosyne/docs/muses/hypothesis-tests/.
Instructions for setting up Argot server may be found at
https://grammatech.gitlab.io/Mnemosyne/docs/usage/.
