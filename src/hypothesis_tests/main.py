#!/usr/bin/env python3
"""
LSP server for type-driven hypothesis test creation.
"""

import argparse
import ast
import logging
import multiprocessing

import astunparse

from typing import List, Set

from pygls.lsp.methods import CODE_ACTION, TEXT_DOCUMENT_DID_OPEN
from pygls.server import LanguageServer
from pygls.lsp.types import CodeAction, CodeActionParams
from pygls.lsp.types import DidOpenTextDocumentParams
from pygls.workspace import Document
from pygls.protocol import LanguageServerProtocol

import hypothesis_tests.lsp as lsp
import hypothesis_tests.lib as lib


def main():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=3033,
                        help='Port to listen for LSP requests on.')
    parser.add_argument('-s',
                        '--stdio',
                        action='store_true',
                        help='Run LSP server in STDIO mode.')
    parser.add_argument('-f',
                        '--file',
                        type=str,
                        help='Create tests for the file given in batch mode.')
    parser.add_argument('-v',
                        '--verbose',
                        help='Increase output verbosity',
                        action='store_const',
                        const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-n',
                        '--num-threads',
                        type=int,
                        default=multiprocessing.cpu_count() // 3,
                        help='Number of server threads.')

    args = parser.parse_args()
    logging.basicConfig(level=args.verbose)

    if args.file:
        with open(args.file, 'r') as f:
            root = ast.parse(f.read())

        imports = lib.hypothesis_imports()
        tests = [
            test for ast in root.body
            if (test := lib.hypothesis_test(ast, root))
        ]
        root.body = imports + root.body + tests \
                    if tests \
                    else root.body
        print(astunparse.unparse(root.body))
    else:
        class NonTerminatingLanguageServerProtocol(LanguageServerProtocol):
            """
            language server protocol implementation which ignores exit/shutdown
            messages from the client
            """

            def bf_exit(self, *args):
                pass

            def bf_shutdown(self, *args):
                pass

            def connection_lost(self, *args):
                pass

        if args.stdio:
            server = LanguageServer()
        else:
            server = LanguageServer(
                protocol_cls=NonTerminatingLanguageServerProtocol
            )

        @server.command('generate_hypothesis_test')
        def generate_hypothesis_test(ls, cmd_args):
            # Parse the source text and filter function ASTs.
            uri = cmd_args[0]
            names = cmd_args[1]
            document = server.workspace.get_document(uri)
            edit = lsp.generate_tests(document, names, args.num_threads)
            if edit:
                server.apply_edit(edit)
            else:
                msg = f"@{names[0]}" if len(names) == 1 else "all"
                msg = f"{msg} - hypothesis test(s) could not be generated"
                server.show_message(msg)

        # This should not be necessary.
        @server.feature(TEXT_DOCUMENT_DID_OPEN)
        def didopen(params: DidOpenTextDocumentParams):
            server.workspace.put_document(params.text_document)

        @server.feature(CODE_ACTION)
        def codeactions(params: CodeActionParams) -> List[CodeAction]:
            def document() -> Document:
                '''
                Return the document from which the client request originated.
                '''
                workspace = server.workspace
                return workspace.get_document(params.text_document.uri)

            def is_tested(func: ast.AST, fnames: Set[str]) -> bool:
                '''
                Return True if a hypothesis test for FUNC exists in FNAMES.
                '''
                test_name = 'test_%s' % func.name
                return test_name in fnames

            def is_hypothesis_test(func: ast.AST) -> bool:
                '''
                Return True if FUNC is a hypothesis test.
                '''
                def is_hypothesis(decorator: ast.AST) -> bool:
                    return isinstance(decorator, ast.Call) and \
                           decorator.func.id == 'given'

                decorators = func.decorator_list
                return decorators and any(is_hypothesis(d) for d in decorators)

            def get_hypothesis_test(func: ast.AST,
                                    funcs: List[ast.AST]) -> ast.FunctionDef:
                '''
                Retrieve the hypothesis test for FUNC from FUNCS if one exists.
                If FUNC itself is a hypothesis test, return FUNC.
                '''
                if is_hypothesis_test(func):
                    return func
                else:
                    test_name = 'test_%s' % func.name
                    candidates = [
                        f for f in funcs
                        if is_hypothesis_test(f) and f.name == test_name
                    ]
                    return candidates[0] if len(candidates) == 1 else None

            def in_range(func: ast.AST) -> bool:
                '''
                Return True if FUNC intersects the range selected by the client.
                '''
                decorators = func.decorator_list
                func_beg = getattr(func, 'lineno', -1) \
                           if not decorators \
                           else getattr(decorators[0], 'lineno', -1)
                func_end = getattr(func, 'end_lineno', -1)
                range_beg = params.range.start.line + 1
                range_end = params.range.end.line + 1

                # 1. The beginning of the range falls within the function or
                # 2. The end of the range falls within the function or
                # 3. The beginning of the function falls within the range or
                # 4. The end of the function falls within within the range
                return ((func_beg <= range_beg and range_beg <= func_end)
                        or (func_beg <= range_end and range_end <= func_end)
                        or (range_beg <= func_beg and func_beg <= range_end)
                        or (range_beg <= func_end and func_end <= range_end))

            # Parse the source text.
            try:
                root = ast.parse(document().source)
            except SyntaxError:
                return []

            # Filter function ASTs.
            root_funcs = list(filter(lambda f: lib.is_function(f), root.body))
            root_func_names = set(f.name for f in root_funcs)

            # Filter functions which are:
            # 1) In the range selected by the client
            # 2) Not currently tested with a hypothesis test
            # 3) Not a hypothesis test themselves
            funcs = filter(lambda f: in_range(f), root_funcs)
            funcs = filter(lambda f: not is_tested(f, root_func_names), funcs)
            funcs = filter(lambda f: not is_hypothesis_test(f), funcs)
            funcs = list(funcs)

            # Filter functions which are in the range selected by the client.
            # Map over the selected functions, retrieving the corresponding
            # hypothesis test or if the selected function is a hypothesis
            # test, the function itself.  Filter any duplicates, setting
            # tests to a list of existing hypothesis tests corresponding
            # to the selected code region.
            tests = filter(lambda f: in_range(f), root_funcs)
            tests = map(lambda f: get_hypothesis_test(f, root_funcs), tests)
            tests = list(set(filter(None, tests)))

            # Create commands for those functions for which
            # a type-driven hypothesis test may be created.
            # If applicable, also create a single command
            # to create type-driven hypothesis tests for all
            # selected functions.
            test_commands = lsp.code_actions(
                funcs,
                root=root,
                document=document(),
                code_action_fn=lsp.test_command,
                combined_code_action_fn=lsp.combined_test_command,
                num_threads=args.num_threads)

            # Create code actions inserting or removing an @example
            # decorator for those hypothesis tests selected where a
            # falsifying example has been found or corrected. If
            # applicable, also create a single code action to update
            # all @example decorators in selected hypothesis tests.
            example_actions = lsp.code_actions(
                tests,
                root=root,
                document=document(),
                code_action_fn=lsp.example_code_action,
                combined_code_action_fn=lsp.combined_example_code_action,
                num_threads=args.num_threads)

            # Return the concatenation of the hypothesis test and example
            # decorator commands and code actions.
            return test_commands + example_actions

        if args.stdio:
            server.start_io()
        else:
            server.start_tcp('0.0.0.0', args.port)


if __name__ == '__main__':
    main()
