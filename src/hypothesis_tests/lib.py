"""
Library for creating hypothesis tests of function ASTs, creating hypothesis
import ASTs, and creating hypothesis @example decorators from failing
unit tests.
"""

import ast
import astunparse
import re
import subprocess
import sys
import tempfile

from typing import Any, List, Optional, Tuple


def is_function(func: ast.AST) -> bool:
    """
    Return True if FUNC is a function AST.
    """
    return isinstance(func, ast.AsyncFunctionDef) or \
           isinstance(func, ast.FunctionDef)


def hypothesis_imports() -> List[ast.AST]:
    """
    Return a list of imports required for type-driven hypothesis testing.
    """
    return [
        ast.ImportFrom(module='hypothesis',
                       names=[
                           ast.alias(name='given', asname=None),
                           ast.alias(name='example', asname=None)
                       ],
                       level=0),
        ast.ImportFrom(module='hypothesis.extra.dpcontracts',
                       names=[ast.alias(name='fulfill', asname=None)],
                       level=0),
        ast.Import([ast.alias(name='hypothesis.strategies', asname='st')])
    ]


def hypothesis_test(a: ast.AST, root: ast.AST) -> Optional[ast.FunctionDef]:
    """
    Create a type-driven hypothesis test for the function AST A in ROOT.
    """
    def arglist(args: ast.arguments) -> List[ast.AST]:
        """
        Return the arguments in ARGS as a list.
        """
        return (args.posonlyargs or []) + \
               (args.args or []) + \
               (args.kwonlyargs or []) + \
               (args.kwarg or []) + \
               (args.vararg or [])

    def strip(args: List[ast.AST]) -> List[ast.AST]:
        """
        Return the arguments in ARGS stripped of annotations and type comments.
        """
        return [ast.arg(arg.arg, annotation=None) for arg in (args or [])]

    def hypothesis_strategy(param: ast.arg) -> Optional[ast.keyword]:
        """
        Create a strategy from the hypothesis.strategies module
        corresponding to the given parameter PARAM.

        A complete listing of explicit strategies may be found at
        https://hypothesis.readthedocs.io/en/latest/data.html#core-strategies.
        """
        PARAMETERIZED_TYPES = {
            'List': 'lists',
            'Set': 'sets',
            'Dict': 'dictionaries',
            'Iterable': 'iterables',
            'Tuple': 'tuples',
        }

        BASE_TYPES = {
            'int': 'integers',
            'float': 'floats',
            'bool': 'booleans',
            'bytes': 'binary',
            'str': 'text',
            'ByteString': 'binary',
            'Text': 'text',
            'datetime': 'datetimes',
            'time': 'times',
            'timedelta': 'timedeltas',
            'Decimal': 'decimals',
            'Fraction': 'fractions',
            'Random': 'randoms',
            'UUID': 'uuids',
        }

        def keyword(strategy: str,
                    type_params: List[ast.AST] = []) -> ast.keyword:
            """
            Create a keyword AST defining the hypothesis strategy
            for this parameter.
            """
            call = ast.Call(func=ast.Attribute(value=ast.Name('st'),
                                               attr=strategy),
                            args=type_params,
                            keywords=[])
            return ast.keyword(arg=param.arg, value=call)

        def helper(annotation: ast.AST) -> Optional[ast.keyword]:
            """
            Recursive helper function for creating a hypothesis
            strategy from the given type ANNOTATION.
            """
            if isinstance(annotation, ast.Subscript):
                value = annotation.value
                slce = annotation.slice
                strategy = None
                type_params = []

                if isinstance(value, ast.Attribute):
                    strategy = PARAMETERIZED_TYPES.get(value.attr)
                elif isinstance(value, ast.Name):
                    strategy = PARAMETERIZED_TYPES.get(value.id)

                if isinstance(slce.value, ast.Tuple):
                    type_params = [
                        helper(elt).value for elt in slce.value.elts
                    ]
                elif isinstance(slce.value, ast.Name) or \
                     isinstance(slce.value, ast.Attribute):
                    type_params = [helper(slce.value).value]

                if strategy and type_params \
                   and not any(t is None for t in type_params):
                    return keyword(strategy, type_params)
            elif isinstance(annotation, ast.Name):
                strategy = BASE_TYPES.get(annotation.id, None)
                if strategy:
                    return keyword(strategy)
            elif isinstance(annotation, ast.Attribute):
                strategy = BASE_TYPES.get(annotation.attr, None)
                if strategy:
                    return keyword(strategy)
            elif isinstance(annotation, ast.Constant) and not annotation.value:
                return keyword('none')
            else:
                return None

        return helper(param.annotation)

    def strategy_decorator(func: ast.AST) -> Optional[ast.Call]:
        """
        Create a complete decorator with hypothesis strategies
        for each parameter in FUNC.
        """
        strategies = [hypothesis_strategy(arg) for arg in arglist(func.args)]

        if strategies and not any(s is None for s in strategies):
            return ast.Call(func=ast.Name('given'),
                            args=[],
                            keywords=strategies)

    def has_dpcontract(func: ast.AST) -> bool:
        '''
        Return TRUE if FUNC contains a dpcontracts precondition decorator.
        '''
        decorators = func.decorator_list
        decorators = filter(lambda d: isinstance(d, ast.Call), decorators)
        decorators = filter(lambda d: d.func.id == 'require', decorators)
        return len(list(decorators)) > 0

    def create_hypothesis_test(func: ast.AST) -> Optional[ast.FunctionDef]:
        """
        Create a type-driven hypothesis test for the function FUNC.
        """
        if test_decorator := strategy_decorator(func):
            # Create the test function.
            test_name = 'test_%s' % func.name
            test_args = ast.arguments(posonlyargs=strip(func.args.posonlyargs),
                                      args=strip(func.args.args),
                                      kwonlyargs=strip(func.args.kwonlyargs),
                                      kwarg=strip(func.args.kwarg),
                                      vararg=strip(func.args.vararg),
                                      defaults=[],
                                      kwdefaults=[])
            test_call_args = [ast.Name(arg.arg) for arg in arglist(test_args)]
            test_call_func = ast.Call(func=ast.Name('fulfill'),
                                      args=[ast.Name(func.name)],
                                      keywords=[]) \
                             if has_dpcontract(func) \
                             else ast.Name(func.name)
            test_call = ast.Call(func=test_call_func,
                                 args=test_call_args,
                                 keywords=[])
            test_body = [ast.Expr(value=test_call)]
            test_func = ast.FunctionDef(name=test_name,
                                        args=test_args,
                                        body=test_body,
                                        decorator_list=[test_decorator])

            # Add @example decorators if falsifying examples are
            # found via testing.
            examples = falsifying_examples(test_func, root)
            if examples:
                example_decorators = [
                    hypothesis_example_decorator(example)
                    for example in examples
                ]
                test_func.decorator_list.extend(example_decorators)

            return test_func

    if is_function(a):
        return create_hypothesis_test(a)


def hypothesis_example_decorator(example: List[Tuple]) -> ast.Call:
    '''
    Create an hypothesis @example decorator from the given falsifying
    EXAMPLE list of (argument, value) pairs.
    '''
    def create_keyword(arg: str, value: Any) -> ast.keyword:
        '''
        Create a keyword AST from the given ARG and VALUE.
        '''
        return ast.keyword(arg=arg, value=ast.Name(str(value)))

    keywords = [create_keyword(arg, value) for arg, value in example]
    return ast.Call(func=ast.Name('example'), args=[], keywords=keywords)


def falsifying_examples(test: ast.AST,
                        root: ast.AST,
                        seed: int = 0) -> List[List[Tuple]]:
    """
    Run the given hypothesis TEST in ROOT, return a list of (argument, value) lists
    representing the falsifying examples discovered by hypothesis.
    """
    with tempfile.TemporaryDirectory() as d:
        with tempfile.NamedTemporaryFile(mode='w', suffix='.py') as f:
            # Add TEST to ROOT's body if it is not yet present
            body = root.body
            if test not in body:
                body = [hypothesis_imports()] + body + [test]
            module = ast.Module(body=body)

            # Write the source code to file.
            f.write(astunparse.unparse(module))
            f.flush()

            # Run the TEST given under pytest.
            command = [
                'pytest',
                '--timeout=30',
                '--hypothesis-seed',
                str(seed), '-k', test.name, f.name
            ]
            proc = subprocess.Popen(command,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    cwd=d)
            stdout = proc.communicate()[0]
            lines = stdout.decode(sys.stdout.encoding).split('\n')
            lines = [line for line in lines if line]

            # Check to ensure TEST was executed.
            if not lines or proc.poll() == 5:
                raise RuntimeError("Hypothesis test was not executed.")

            # Parse the output for falsifying examples discovered by the
            # hypothesis library.
            results = []
            lines = iter(lines)
            for line in lines:
                # Example hypothesis outputs:
                #
                # Falsifying example: test_delete_ones(
                #     l=[1, 0],
                # )
                #
                # Falsifying explicit example: test_delete_ones(
                #     l=[1, 0],
                # )
                #
                # When an explicit @example decorator is present and failing,
                # the bottom form is given.  The 'Falsifying.*example: (.*)\\('
                # regex captures the function name in the first group.
                # Once the regex is matched, we look for argument=value
                # pairs in the output with the ' +(.*)=(.*),' regex which
                # skips leading whitespace and places the argument in
                # the first group and the value in the second group.

                match = re.match('Falsifying.*example: (.*)\\(', line)
                if match:
                    example = match.group(1)
                    if test.name == example:
                        example_args = []

                        # Parse the keyword/argument pairs in the example.
                        while (line := next(lines)):
                            id_chars = '[A-Za-z0-9_]'
                            for arg in re.findall(f'({id_chars}+)=', line):
                                regex = f'{arg}=(.*?),( +{id_chars}+=|$)'
                                match = re.search(regex, line)
                                value = match.group(1)

                                try:
                                    # Attempt to parse the value using the
                                    # python AST library.
                                    value = ast.literal_eval(value)
                                except ValueError:
                                    # There was an error in parsing the value.
                                    # Generally, this occurs when attempting
                                    # to parse 'inf', '-inf', and 'nan'.
                                    # Massage value to be 'float("inf")',
                                    # 'float("-inf"), or 'float("nan")'
                                    # instead.
                                    non_id = '[^A-Za-z0-9_]'
                                    pattern = f'(^|{non_id})(%s)($|{non_id})'
                                    replace = '\\1float("%s")\\3'

                                    value = re.sub(pattern % 'inf',
                                                   replace % 'inf', value)
                                    value = re.sub(pattern % 'nan',
                                                   replace % 'nan', value)
                                    value = re.sub(pattern % '-inf',
                                                   replace % '-inf', value)

                                pair = (arg, value)
                                example_args.append(pair)
                            else:
                                break

                        # Append the example to the results.
                        results.append(example_args)

            return results
