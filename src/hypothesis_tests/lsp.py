"""
Routines for the creation of Language Server Protocol (LSP) objects
for hypothesis tests.
"""

import ast
import astunparse
import multiprocessing

from itertools import repeat
from typing import Callable, List, Optional, Text, Tuple, Union

from pygls.lsp.types import CodeAction
from pygls.lsp.types import Position, Range
from pygls.lsp.types import TextEdit, TextDocumentEdit, WorkspaceEdit
from pygls.lsp.types import VersionedTextDocumentIdentifier
from pygls.lsp.types import Command
from pygls.workspace import Document

import hypothesis_tests.lib as lib


def text_edit(source: str,
              line: int = 0,
              col: int = 0,
              end_line: int = 0,
              end_col: int = 0) -> TextEdit:
    '''
    Return a LSP TextEdit object for the given SOURCE snippet to be inserted
    at LINE, COL optionally spanning to END_LINE, END_COL.
    '''
    start = Position(line=line, character=col)
    end = Position(line=end_line or line, character=end_col)
    r = Range(start=start, end=end)
    return TextEdit(range=r, newText=source)


def test_edit(func: ast.AST, root: ast.AST,
              document: Document):
    # Create the hypothesis test AST for FUNC, if possible.
    test = lib.hypothesis_test(func, root)

    if test:
        # Create an LSP TextEdit object for the test.
        test_edit = text_edit(astunparse.unparse(test),
                              getattr(func, 'end_lineno', 0))

        return test_edit


def generate_tests(document, names, num_threads=1) -> Optional[WorkspaceEdit]:
    root = ast.parse(document.source)
    root_funcs = list(filter(lambda f: lib.is_function(f), root.body))
    fs = list(filter(lambda f: f.name in names, root_funcs))

    if len(fs) == 0:
        return

    num_threads = min(num_threads, len(fs))

    # Create LSP TextEdit objects for the imports required by
    # the test if they are not present.
    imports = lib.hypothesis_imports()
    import_text = astunparse.unparse(imports[0]).strip()
    if import_text not in document.source:
        import_edits = [
            text_edit(astunparse.unparse(i).lstrip())
            for i in imports
        ]
        import_edits.append(text_edit('\n'))
    else:
        import_edits = []

    with multiprocessing.Pool(num_threads) as p:
        edits = p.starmap(test_edit,
                          zip(fs, repeat(root), repeat(document)))
        edits = [edit for edit in edits if edit is not None]

    if edits:
        text_document = VersionedTextDocumentIdentifier(
            uri=document.uri,
            version=document.version
        )
        doc_edit = TextDocumentEdit(text_document=text_document,
                                    edits=edits+import_edits)
        return WorkspaceEdit(document_changes=[doc_edit])


def test_command(func: ast.AST, root: ast.AST,
                 document: Document) -> Optional[Command]:
    '''
    Return a LSP Command object for inserting a hypothesis test of FUNC
    (from the body of ROOT) into DOCUMENT.
    '''

    # Return the completed code action.
    return Command(title='Hypothesis Test @%s' % (func.name),
                   command='generate_hypothesis_test',
                   arguments=[document.uri, [func.name]])


def combined_test_command(commands: List[Command],
                          document: Document) -> Command:
    '''
    Return a LSP Command object for inserting all the hypothesis tests
    defined within the given code ACTIONS into DOCUMENT.
    '''
    assert len(commands) > 1

    names = []
    for command in commands:
        names.extend(command.arguments[1])

    # Return the completed code action.
    return Command(title='Hypothesis Test (all)',
                   command="generate_hypothesis_test",
                   arguments=[document.uri, names])


def example_code_action(test: ast.FunctionDef, root: ast.AST,
                        document: Document) -> Optional[CodeAction]:
    '''
    Return a LSP CodeAction object for inserting an falsifying @example
    decorator of FUNC (from the body of ROOT) into DOCUMENT.
    '''
    def get_example_decorators(test: ast.FunctionDef) -> List[ast.Call]:
        '''
        Return the @example decorators for TEST if they exist.
        '''
        decorators = test.decorator_list
        decorators = filter(lambda d: isinstance(d, ast.Call), decorators)
        decorators = filter(lambda d: d.func.id == 'example', decorators)
        return list(decorators)

    def parse_example_decorator(decorator: ast.Call) -> List[Tuple]:
        '''
        Parse the given @example DECORATOR into a list of (argument, value)
        pairs representing a falsifying example discovered by hypothesis.
        '''
        result = []

        for keyword in decorator.keywords:
            arg = keyword.arg
            try:
                value = ast.literal_eval(keyword.value)
            except ValueError:
                value = astunparse.unparse(keyword.value)

            if isinstance(value, str):
                value = value.strip().replace("'", '"')

            result.append((arg, value))

        return result

    def decorators_text(decorators: List[ast.Call]) -> Text:
        '''
        Return the source text associated with the given list of DECORATORS.
        '''
        text = [
            '@%s' % (astunparse.unparse(decorator)) for decorator in decorators
        ]
        text = ''.join(text)

        return text

    # Retrieve the falsifying examples currently present on the test
    # if they exist.
    current_decorators = get_example_decorators(test)
    current_examples = [
        parse_example_decorator(decorator) for decorator in current_decorators
    ]

    # Execute the hypothesis test to retrieve a new falsifying examples
    # if they exist.
    try:
        new_examples = lib.falsifying_examples(test, root)
        new_decorators = [
            lib.hypothesis_example_decorator(example) for example in new_examples
        ]
    except RuntimeError:
        # Error running the tests, do not update current examples.
        new_examples = current_examples

    # Build the code action title and edit.
    title = None
    decorator_edit = None
    if current_examples != new_examples:
        if current_examples == []:
            # We are inserting new decorators.
            title = f'Insert falsifying examples @{test.name}'
            line = getattr(test, 'lineno') - 1
            text = decorators_text(new_decorators)
            decorator_edit = text_edit(text, line)
        elif new_examples == []:
            # We are removing corrected decorators.
            title = f'Remove corrected examples @{test.name}'
            line = getattr(current_decorators[0], 'lineno') - 1
            end_line = getattr(current_decorators[-1], 'end_lineno')
            decorator_edit = text_edit('', line=line, end_line=end_line)
        else:
            # We are updating existing decorators.
            title = f'Update falsifying examples @{test.name}'
            line = getattr(current_decorators[0], 'lineno') - 1
            end_line = getattr(current_decorators[-1], 'end_lineno')
            text = decorators_text(new_decorators)
            decorator_edit = text_edit(text, line=line, end_line=end_line)

    # Return the completed code action if the example has changed.
    if current_examples != new_examples:
        # Create the intermediate LSP objects required for the
        # code action.
        text_document = VersionedTextDocumentIdentifier(
            uri=document.uri,
            version=document.version)
        edits = [decorator_edit]
        doc_edit = TextDocumentEdit(text_document=text_document, edits=edits)
        workspace_edit = WorkspaceEdit(document_changes=[doc_edit])
        codeAction = CodeAction(title=title, edit=workspace_edit)
        return codeAction


def combined_example_code_action(actions: List[CodeAction],
                                 document: Document) -> CodeAction:
    '''
    Return a LSP CodeAction object aggregrating all of the @example decorator
    updates within the given code ACTIONS to be applied to DOCUMENT.
    '''
    assert len(actions) > 1

    # Build a list of edits for adding/removing/modifying all
    # @example decorators.
    edits = []
    for action in reversed(actions):
        edits.extend(action.edit.documentChanges[0].edits)

    # Create the intermediate LSP objects required for the
    # code action.
    text_document = VersionedTextDocumentIdentifier(uri=document.uri,
                                                    version=document.version)
    doc_edit = TextDocumentEdit(text_document=text_document, edits=edits)
    workspace_edit = WorkspaceEdit(document_changes=[doc_edit])

    # Return the completed code action.
    return CodeAction(title='Update falsifying examples (all)',
                      edit=workspace_edit)


def code_actions(asts: List[ast.AST],
                 root: ast.AST,
                 document: Document,
                 code_action_fn: Callable,
                 combined_code_action_fn: Callable,
                 num_threads: int = 1) -> List[Union[Command, CodeAction]]:
    '''
    Create code actions for the given list of ASTS in ROOT/DOCUMENT by calling
    CODE_ACTION_FN to create code actions for each AST and
    COMBINED_CODE_ACTION_FN to create an aggregate code action from the
    individual actions created.
    '''
    if num_threads <= 1:
        actions = map(lambda ast: code_action_fn(ast, root, document), asts)
    else:
        with multiprocessing.Pool(num_threads) as p:
            actions = p.starmap(code_action_fn,
                                zip(asts, repeat(root), repeat(document)))

    actions = list(filter(None, actions))
    if len(actions) > 1:
        actions.insert(0, combined_code_action_fn(actions, document))
    return actions
